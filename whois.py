#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import json


def whois(domain):
    url = 'http://websiteinfo.herokuapp.com/v1/domain/%s' % domain
    response = requests.get(url)
    data = json.loads(response.text)

    info = {}

    if data["error"] != 0:
        info["error"] = data["domain"]["message"]
        if data["error"] == 1:
            return info

    values = data["domain"]
    from pprint import pprint
    pprint(values)
    print

    if values.get("data"):
        if values["data"].get("person"):
            info["person"] = values["data"].get("person")
        else:
            info["person"] = []
        if values["data"].get("organization"):
            info["organization"] = values["data"].get("organization")
        else:
            info["organization"] = []
        if values["data"].get("contact"):
            info["contact"] = values["data"].get("contact")
        else:
            info["contact"] = []
        if values["data"].get("dateRegistered"):
            info["date_registered"] = values["data"].get("dateRegistered")
        else:
            info["date_registered"] = []

    info["message"] = values["message"].strip()
    info["raw_data"] = values["raw_data"]
    return info

if __name__ == "__main__":
    import sys
    from pprint import pprint
    pprint(whois(sys.argv[1]))
